import numpy as np 
from scipy.integrate import odeint
import matplotlib.pyplot as plt
def Tank(h,t):
    F0 = 1 #m^3/min Caudal entrada
    A = 10 #m^2 Area del Circulo(base)
    beta =2 #Factor a/A
    dhdt= F0/A - beta/A *np.sqrt(h) #Funcion en m/min
    return dhdt 
#Vector tiempo
t=np.linspace(0,40)
h1=np.linspace(0,0.25) #Vector altura según la EDO
#Condición inicial
h0=0
h=odeint(Tank,h0,t)
r=1.78#Radio Tanque
dv= np.pi*(r**2)*h1 #Volumen m^3
print(dv)
g=9.8 #Gravedad m^2/seg
A=10 # #m^2 Area del Circulo(base)
dq=np.sqrt(2*g*h1)*A #Caudal m^3/min
#Figura
plt.plot(t,h)
plt.xlabel('Tiempo,[Minutos]') #Nombre Eje X
plt.ylabel('Altura Tanque,[Metros]') #Nombre eje Y
plt.show()
#Figura
plt.plot(t,dv)
plt.xlabel('Tiempo,[Minutos]') #Nombre Eje X
plt.ylabel('Volumen Tanque,[Metros^3]') #Nombre eje Y
plt.show()
#figura
plt.plot(t,dq)
plt.xlabel('Tiempo,[Minutos]')#Nombre Eje X
plt.ylabel('Caudal,[Metros^3/min]')#Nombre Eje Y
plt.show()