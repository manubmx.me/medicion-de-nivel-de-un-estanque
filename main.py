import numpy as np 
from scipy.integrate import odeint
import matplotlib.pyplot as plt
def Tank(h,t):
    F0 = 1 #m^3/min Caudal entrada
    A = 10 #m^2 Area del Circulo(base)
    beta =2 #Factor a/A
    dhdt= F0/A - beta/A *np.sqrt(h) #Funcion en m/min
    return dhdt 
#Vector tiempo
t=np.linspace(0,40)
#Condición inicial
h0=0
h=odeint(Tank,h0,t)
def Volumen(h,t):
    r=1.78 #Radio
    dvdt= np.pi*(r**2)*h1#Funcion en m/min
    return dvdt 
#Condición inicial
h1=np.linspace(0,0.25)
v=odeint(Volumen,h1,t)
def Caudal(h,t):
    g=9.8
    A=10
    dqdt=np.sqrt(2*g*h1)*A
    return dqdt
h1=np.linspace(0,0.25) 
q=odeint(Caudal,h1,t)
#Figura
plt.plot(t,h)
plt.xlabel('Tiempo,[Minutos]') #Nombre Eje X
plt.ylabel('Altura Tanque,[Metros]') #Nombre eje Y
plt.show()
#Figura
plt.plot(t,v)
plt.xlabel('Tiempo,[Minutos]') #Nombre Eje X
plt.ylabel('Volumen Tanque,[Metros^3]') #Nombre eje Y
plt.show()
#figura
plt.plot(t,q)
plt.xlabel('Tiempo,[Minutos]')
plt.ylabel('Caudal,[Metros^3/seg]')
plt.show